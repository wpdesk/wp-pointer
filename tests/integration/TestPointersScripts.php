<?php

use \WPDesk\Pointer\PointersScripts;

class TestPointersScripts extends WP_UnitTestCase
{

    const WP_DEFAULT_PRIORITY = 10;

    public function testHooks()
    {
        $pointerScripts = new PointersScripts();
        $pointerScripts->hooks();

        $this->assertEquals(
            self::WP_DEFAULT_PRIORITY,
            has_action('admin_enqueue_scripts', [$pointerScripts, 'enqueueScripts'])
        );

        remove_action('admin_enqueue_scripts', [$pointerScripts, 'enqueueScripts']);
    }

    public function testLoadScripts()
    {
        $pointerScripts = new PointersScripts();
        $pointerScripts->hooks();
        do_action('admin_enqueue_scripts');

        $registeredScripts = wp_scripts()->queue;
        $this->assertContains('wp-pointer', $registeredScripts, 'Script not registered!');

        $registeredStyles = wp_styles()->queue;
        $this->assertContains('wp-pointer', $registeredStyles, 'Style not registered!');

        remove_action('admin_enqueue_scripts', [$pointerScripts, 'enqueueScripts']);
        wp_dequeue_script('wp-pointer');
        wp_dequeue_style('wp-pointer');
    }

    public function testLoadScriptsWithParameter()
    {
        $pointerScripts = new PointersScripts(array('test'));
        $pointerScripts->hooks();
        do_action('admin_enqueue_scripts', 'test');

        $registeredScripts = wp_scripts()->queue;
        $this->assertContains('wp-pointer', $registeredScripts, 'Script not registered!');

        $registeredStyles = wp_styles()->queue;
        $this->assertContains('wp-pointer', $registeredStyles, 'Style not registered!');

        remove_action('admin_enqueue_scripts', [$pointerScripts, 'enqueueScripts']);
        wp_dequeue_script('wp-pointer');
        wp_dequeue_style('wp-pointer');

        do_action('admin_enqueue_scripts', 'test1');

        $registeredScripts = wp_scripts()->queue;
        $this->assertNotContains('wp-pointer', $registeredScripts, 'Script registered!');

        $registeredStyles = wp_styles()->queue;
        $this->assertNotContains('wp-pointer', $registeredStyles, 'Style registered!');

        remove_action('admin_enqueue_scripts', [$pointerScripts, 'enqueueScripts']);
        wp_dequeue_script('wp-pointer');
        wp_dequeue_style('wp-pointer');
    }

}
