[![pipeline status](https://gitlab.com/wpdesk/wp-pointer/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-pointer/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-pointer/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-pointer/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-pointer/v/stable)](https://packagist.org/packages/wpdesk/wp-pointer) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-pointer/downloads)](https://packagist.org/packages/wpdesk/wp-pointer) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-pointer/v/unstable)](https://packagist.org/packages/wpdesk/wp-pointer) 
[![License](https://poser.pugx.org/wpdesk/wp-pointer/license)](https://packagist.org/packages/wpdesk/wp-pointer) 


WordPress Library to display pointer messages in admin area.
===================================================

## Project documentation

PHPDoc: https://wpdesk.gitlab.io/wp-pointer/index.html 
