<?php

use \WPDesk\Pointer\PointersScripts;
use \WPDesk\Pointer\PointerMessage;
use \WPDesk\Pointer\PointerConditions;
use \WPDesk\Pointer\PointerPosition;

class TestPointerMessage extends WP_UnitTestCase
{

    const WP_DEFAULT_PRIORITY = 10;

    private function preparePointerMessage()
    {
        return new PointerMessage(
            'id',
            'anchor',
            'title',
            'content',
            new PointerPosition(PointerPosition::LEFT, PointerPosition::LEFT)
        );
    }

    public function testHooks()
    {
        $pointerMessage = $this->preparePointerMessage();

        $this->assertEquals(
            self::WP_DEFAULT_PRIORITY,
            has_action('admin_print_footer_scripts', [$pointerMessage, 'maybeRenderJavascript'])
        );
    }

    public function testRemoveAction()
    {
        $pointerMessage = $this->preparePointerMessage();

        $this->assertEquals(
            self::WP_DEFAULT_PRIORITY,
            has_action('admin_print_footer_scripts', [$pointerMessage, 'maybeRenderJavascript'])
        );

        $pointerMessage->removeAction();

        $this->assertFalse(
            has_action('admin_print_footer_scripts', [$pointerMessage, 'maybeRenderJavascript'])
        );
    }

    public function testRenderJavaScript()
    {
        $pointerMessage = $this->preparePointerMessage();

        $this->expectOutputString('<script type="text/javascript">
    jQuery(document).ready( function($) {
        if(typeof(jQuery().pointer) != \'undefined\') {
            $(\'anchor\').pointer({
                pointerWidth: 320,
                content: "<h3>title<\/h3><p id=\"wpdesk_pointer_content_id\">content<\/p>",
                position: {"edge":"left","align":"left"},
                close: function() {
                    $(\'#wpdesk_pointer_content_id\').remove();
                    $.post(ajaxurl, {
                        pointer: \'id\',
                        action: \'dismiss-wp-pointer\'
                    });
                },
            }).pointer(\'open\');
        }
    });
</script>
');

        $pointerMessage->renderJavaScript();
    }

}
