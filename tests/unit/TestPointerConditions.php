<?php

use \WPDesk\Pointer\PointerConditions;
use \WP_Mock\Tools\TestCase;

class TestPointerConditions extends TestCase
{

    public function setUp()
    {
        WP_Mock::setUp();
    }

    public function tearDown()
    {
        WP_Mock::tearDown();
    }

    public function testAreConditionsMetNoParameters()
    {
        $pointerConditions = new PointerConditions();
        $this->assertTrue($pointerConditions->areConditionsMet());
    }

    public function testAreConditionsMetScreen()
    {
        $testScreen = 'testScreen';
        $pointerConditions = new PointerConditions($testScreen);

        $fakeCurrentScreen = new stdClass();
        $fakeCurrentScreen->id = $testScreen;

        WP_Mock::userFunction('get_current_screen', array(
            'times' => 1,
            'return' => $fakeCurrentScreen,
        ));

        $this->assertTrue($pointerConditions->areConditionsMet());
    }

    public function testAreConditionsMetCapability()
    {
        $testCapability = 'testCapability';
        $pointerConditions = new PointerConditions(null, $testCapability);

        WP_Mock::userFunction('current_user_can', array(
            'times' => 1,
            'return' => true,
        ));

        $this->assertTrue($pointerConditions->areConditionsMet());
    }

}
