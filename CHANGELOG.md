## [2.0.1] - 2021-09-29
### Fixed
- allowed wp-builder v 2

## [2.0.0] - 2020-07-14
### Fixed
- pointer position relative to DOM element

## [1.0.3] - 2020-06-30
### Added
- additional pointer CSS
